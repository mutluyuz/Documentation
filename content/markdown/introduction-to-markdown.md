---
eleventyNavigation:
  key: IntroductionToMarkdown
  title: Introduction to Markdown
  parent: Markdown
  order: 20
---

Markdown files are basically a normal text files. The file extension `.md` specifies that a file can be rendered as Markdown.
You can also use Markdown in many object of the Codeberg platform (Issues, Pull-Requests, etc.).

## Text section

To write a markdown file, simply write the text into a text editor of your choice.
Markdown does not consider single line breaks as start of a new paragraph. 
You can write all your text into one long line or introduce a new line every once in a while.
It is common practice to introduce a new line at about 80 characters to enable users to easily read the plain un-rendered version of the markdown file.
It is however recommended to make a line break in Markdown at logical steps, e.g. at the end of a sentence.
It makes diffs easier to understand, as the context of the complete sentence is preserved. 

If you want to start a new paragraph, use two or more empty new lines to separate the text.
Beware that in the Gitea rendering, text in repos and comment fields the linebreaks are rendered differently.
For example a simple line break is considered in the comments and leads to a new paragraph. 

### Highlighting text sections

In text sections it is possible to highlight passages using **bold** and *italics*.

### Bold

To mark a text as bold use two stars at the start of the section you want to highlight `**`
At the end of the section to be highlighted add another two stars `**`.
Alternatively you can use two underline characters `__` at the beginning and the end of the section
to get the same effect

Example:

```
This is a **highlighted text**.
```

The example gets rendered as:

This is a **highlighted text**.

```
This is also __highlighted text__.
```

The example gets rendered as:

This is also __highlighted text__.

### Italics

To mark a text in italics use one stars at the start of the section you want to highlight `*`
At the end of the section to be highlighted add another two stars `*`.
Alternatively you can use one underline character `_` at the beginning and the end of the section
to get the same effect

Example:

```
This is a *highlighted text*.
```

The example gets rendered as:

This is a *highlighted text*.

```
This is also _highlighted text_.
```

The example gets rendered as:

This is also _highlighted text_.

## Gitea specifics

### Emoticons
Text may contain references to emoticons which are then rendered as a small image.
These are marked using a colon `:`, followed by the identifier of the emoticon to use, followed by another colon `:`.

Examples of the emoticons are: `:codeberg:` leading to <img src="https://codeberg.org/assets/img/emoji/codeberg.png" class="codeberg-design" style="border-style:none;width:1em;height:1em" alt="The Codeberg mountain" /> and `:gitea:` rendered as <img src="https://codeberg.org/assets/img/emoji/gitea.png" class="codeberg-design" style="border-style:none;height:1em;width=1em" alt="The Gitea tea cup" />. 

### Referencing issue

Issues in Codeberg/Gitea can be referenced in the comments of an issue or a pull request by using a hash mark `#` followed by the number of the issue.
The renderer will then include a link to the referenced issue into the comment.
Additionally, a ping back link to the comment containing the reference will be added to the issues referenced in this way. 

### Checkboxes

You can add checkboxes to comments by using square brackets containing a space `[ ]`. These can be checked/unchecked later without editing the comment.
This can for example be useful when creating a Todo list.

### Mermaid diagrams

Gitea can render [Mermaid diagrams](https://mermaid-js.github.io/mermaid/#/) in issues, pull-requests and comments.

Use the render hint `mermaid` on the preformatted section containing the code of the Mermaid diagram.

E.g.

    ```mermaid
        graph TD;
        A(stuff)-->B[one];
        A-->C[two];
        A-->D[three];
    ```

is rendered to:

![Mermaid Example rendering](/assets/images/markdown/mermaid-example.png)