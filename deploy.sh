#!/bin/bash -ex
npm run build

DEPLOYMENT_REPO=${1:-git@codeberg.org:Codeberg-Infrastructure/static-deployments}
DEPLOYMENT_BRANCH=${2:-docs}
rm -rf pages.git
mkdir pages.git
( cd pages.git && git init -b "$DEPLOYMENT_BRANCH" )
rsync -av _site/* pages.git/
( cd pages.git && git add -A )				## add all generated files
( cd pages.git && git commit -m "Deployment at $(date -u -Is)" )	## commit all
( cd pages.git && git remote add origin "$DEPLOYMENT_REPO" )
( cd pages.git && git push -f origin "$DEPLOYMENT_BRANCH" )		## force-push and rewrite (empty) history
